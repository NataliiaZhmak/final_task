using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace NUnitTestProject1_NZhmak

{
    public class Tests
    {
        IWebDriver driver;

        [SetUp]
        public void Setup()
        {
            driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://lipsum.com/");
        }

        [Test]       
        public void checkLoremIpsumOnRUSContainsFishWord()
        {
            MainPage fish = new MainPage(driver);
            fish.SelectRuLanguage();
            Assert.True(fish.GetParagraphText().Contains(""));
        }
        

        public void checkLoremIpsumGeneratesPositiveTest()
        { 
            MainPage first = new MainPage(driver);
            first.ClickGenerateLoremButton();
            Assert.True(first.GetFirstParagraphText().StartsWith("Lorem ipsum dolor sit amet, consectetur adipiscing elit"));
        }
        

        public void checkLoremIpsumGeneratesWithCorrectWordsAmount()
       {
            MainPage words = new MainPage(driver);
            words.ActivateRadioButtonWords();
            List<string> values = new List<string>();
            values.Add("10");
            values.Add("-1");
            values.Add("0");
            values.Add("5");
            values.Add("20");
            foreach (string i in values)
            {
                words.ChangeParameterAmount(i);
                words.ClickGenerateLoremButton();
                Assert.True(words.GetResultDataText().Contains("{i} words"));
            }
        }
    

        public void checkLoremIpsumGeneratesWithCorrectBytesAmount()
        {
            MainPage bytes = new MainPage(driver);
            bytes.ActivateRadioButtonBytes();
            List<string> bytesValues = new List<string>();
            bytesValues.Add("10");
            bytesValues.Add("0");
            bytesValues.Add("100");
            foreach (string s in bytesValues)
            {
                bytes.ChangeParameterAmount(s);
                bytes.ClickGenerateLoremButton();
                Assert.True(bytes.GetResultBytesText().Contains("{s} bytes"));
            }
         }
        

        public void verifyLoremIpsumGenerationWhenCheckboxDisabledNegativeTest()
    {
            MainPage checkbox = new MainPage(driver);
            checkbox.DisableCheckbox();
            checkbox.ClickGenerateLoremButton();
            Assert.False(checkbox.GetResultCheckboxText().StartsWith("Lorem ipsum"));
       }
    

        public bool CheckRandomTextParagraphsContainWordLorem()
        {
            MainPage random = new MainPage(driver);
            int res = 0;
            for (int n = 1; n++; n < 11)
            {
                random.ClickGenerateLoremButton();
                IList<IWebElement> textDemo = new IList<IWebElement>();
                textDemo.Add(random.SearchTextResult());
                res = res + textDemo.Count();
                n++;
            }
            bool result = (res < 2);
            return result;
        }

        [TearDown]
        public void closeBrowser()
        {
            driver.Close();
        }        
    }
}