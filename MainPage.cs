using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Text;

namespace NUnitTestProject1_NZhmak
{
    public class MainPage
    {
        IWebDriver driver;

        private By LanguageRu = By.XPath("//div[@id='Languages']/descendant::a[@class='ru']");
        private By ParagraphRu = By.XPath("//div[@id='Panes']");
        private By GenerateButton = By.XPath("//input[@id='generate']");
        private By RadioButtonWords = By.XPath("//form[@method='post']/descendant::tr[3]");
        private By AmountField = By.XPath("//div[@id='Panes']/descendant::input[@id='amount']");
        private By RadioButtonBytes = By.XPath("//form[@method='post']/descendant::tr[4]");
        private By StartWithCheckbox = By.XPath("//div[@id='Panes']//input[@id='start']");
        private By ParagraphFirst = By.XPath("//div[@id='lipsum']/p[1]");
        private By ResultData = By.XPath("//div[@id='generated']");
        private By ResultText = By.XPath("//div[@id='lipsum']/p[1]");
        private By ResultSearch = By.XPath("//div[@id='lipsum']/p[contains(text(),'lorem')]");

        public MainPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public void SelectRuLanguage()
        {
            driver.FindElement(LanguageRu).Click();
        }

        public string GetParagraphText()
        {
            string elementText = driver.FindElement(ParagraphRu).Text;
            return elementText;
        }

        public void ClickGenerateLoremButton()
        {
            driver.FindElement(GenerateButton).Click();
        }

        public void implicitlyWait(long timeout)
        {
            WebDriverWait wait = new OpenQA.Selenium.Support.UI.WebDriverWait(driver, TimeSpan.FromSeconds(timeout));
        }

        public string GetFirstParagraphText()
        {
            string paragraphText = driver.FindElement(ParagraphFirst).Text;
            return paragraphText;
        }

        public void ActivateRadioButtonWords()
        {
            driver.FindElement(RadioButtonWords).Click();
        }

        public void ChangeParameterAmount(string s)
        {
            driver.FindElement(AmountField).Clear();
            driver.FindElement(AmountField).SendKeys(s);
        }

        public string GetResultDataText()
        {
            string DataText = driver.FindElement(ResultData).Text;
            return DataText;
        }

        public void ActivateRadioButtonBytes()
        {
            driver.FindElement(RadioButtonBytes).Click();
        }

        public string GetResultBytesText()
        {
            string BytesText = driver.FindElement(ResultData).Text;
            return BytesText;
        }

        public void DisableCheckbox()
        {
            driver.FindElement(StartWithCheckbox).Click();
        }

        public string GetResultCheckboxText()
        {
            string CheckboxText = driver.FindElement(ResultText).Text;
            return CheckboxText;
        }

        public IWebElement SearchTextResult()
        {
            IWebElement res = (IWebElement)driver.FindElements(ResultSearch);
            return res;
        }

    }
}
